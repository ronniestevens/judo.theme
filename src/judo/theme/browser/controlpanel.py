from plone.app.registry.browser.controlpanel import RegistryEditForm
from plone.app.registry.browser.controlpanel import ControlPanelFormWrapper

from judo.theme.interfaces import IJudoThemeSettings
from plone.z3cform import layout
from z3c.form import form

class JudoThemeControlPanelForm(RegistryEditForm):
    form.extends(RegistryEditForm)
    schema = IJudoThemeSettings

JudoThemeControlPanelView = layout.wrap_form(JudoThemeControlPanelForm, ControlPanelFormWrapper)
JudoThemeControlPanelView.label = u"Judo theme settings"