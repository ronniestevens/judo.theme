from Products.CMFCore.interfaces import ISiteRoot
from plone.app.contenttypes.interfaces import IFolder
from Products.CMFCore.utils import getToolByName
from plone.memoize.instance import memoize

# needed for the CSSColorClass
from Products.Five.browser import BrowserView
from zope.component import getUtility
from plone.registry.interfaces import IRegistry
from zope.interface import Interface

from plone import api



class HomepageView(BrowserView):

    def homepageNews(self):
        catalog = api.portal.get_tool('portal_catalog')
        result = []
        for news in catalog(
            {'portal_type': 'News Item',
             'Subject': 'homepage',
             'sort_on': 'Date',
             'sort_order': 'descending'}
            ):
            obj = news.getObject()
            result.append(
                dict(title=news.Title,
                     url=news.getURL(),
                     description=news.Description,
                     date=news.EffectiveDate,
                     newsimage=obj.image,
                     imageurl="%s/@@images/image" % news.getURL())
            )
        return result 

    def homepageEvents(self):
        catalog = api.portal.get_tool('portal_catalog')
        result = []
        for event in catalog(
            {'portal_type': 'Event',
             'Subject': 'homepage',
             'sort_on': 'Date',
             'sort_order': 'descending'}
        ):
            result.append(
                dict(title=event.Title,
                     url=event.getURL(),
                     description=event.Description,
                     date=event.start)
            )
        return result

    def homepagePage(self):
        catalog = api.portal.get_tool('portal_catalog')
        results = catalog(portal_type='Document', Subject='homepage', limit=1)
        return results
    
    def events_rows(self):
        return self.groupby(self.homepageEvents(), groupsize=3)
    
    def news_rows(self):
        return self.groupby(self.homepageNews(), groupsize=3)

    def groupby(self, iterable, groupsize=2):
        """
        Split an iterable [a, b, c, ...]
        into a nested list of tuples [(a, b), (c, ...)]
        where the length of the tuples is governed by groupsize.
     
        The default groupsize results in pairwise grouping:
     
        >>> iterable = [1,2,3,4,5,6]
        >>> groupby(iterable)
        [(1, 2), (3, 4), (5, 6)]
     
        If the number of items in iterable doesn't match the groupsize,
        the last returned group is incomplete:
     
        >>> iterable = [1,2,3,4,5]
        >>> groupby(iterable)
        [(1, 2), (3, 4), (5,)]
     
        We can also group by 3 instead of by 2 items:
     
        >>> iterable = [1,2,3,4,5,6,7]
        >>> groupby(iterable, 3)
        [(1, 2, 3), (4, 5, 6), (7,)]
     
        """
        result = []
        subgroup = []
        for item in iterable:
            subgroup.append(item)
            if len(subgroup) % groupsize == 0:
                result.append(tuple(subgroup))
                subgroup = []
        if len(subgroup) > 0:
            result.append(tuple(subgroup))
        return result
    
    def Banners(self):
            catalog = getToolByName(self.context, 'portal_catalog')
            result = []
            path = '/'.join(self.context.getPhysicalPath())
            for banner in catalog(
                {'portal_type': 'banner',
                 'path': dict(query=path, depth=2),
                 'sort_on': 'Date',
                 'sort_order': 'descending',
                 'review_state': 'published',
                 }):
                obj = banner.getObject()
    
                result.append(
                    dict(url=banner.getURL(),
                         title=banner.Title,
                         description=banner.Description,
                         link=obj.internalLink,
                         linkTitle=obj.internalLinkTitle,
                         video=obj.video,
                         image="%s/@@images/image" % obj.absolute_url()),
                )
            return result
    

class NewsView(BrowserView):

    def newsList(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        result = []
        for news in catalog(
            {'portal_type': 'News Item',
             'sort_on': 'Date',
             'sort_order': 'descending'}
            ):
            result.append(
                dict(title=news.Title,
                     url=news.getURL(),
                     description=news.Description,
                     date=news.EffectiveDate,
                     image="%s/@@images/image" % news.getURL())
            )
        return result 

      
    def news_rows(self):
        return self.groupby(self.newsList(), groupsize=2)

    def groupby(self, iterable, groupsize=2):
        result = []
        subgroup = []
        for item in iterable:
            subgroup.append(item)
            if len(subgroup) % groupsize == 0:
                result.append(tuple(subgroup))
                subgroup = []
        if len(subgroup) > 0:
            result.append(tuple(subgroup))
        return result
    
class colorView(BrowserView):
      
    def getColor(self):
        registry = getUtility(IRegistry)
        if registry != 0:
            color = registry['judo.theme.interfaces.IJudoThemeSettings.themeColor']
            
        return color


