# Zope imports
from Acquisition import aq_inner
from zope.interface import Interface
from Products.CMFCore.interfaces import ISiteRoot

from zope.component import getMultiAdapter
from Products.CMFCore.utils import getToolByName

from plone.app.layout.viewlets import ViewletBase
from plone.memoize.instance import memoize

# access the contact settings
from zope.component import getUtility
from plone.registry.interfaces import IRegistry

# Plone imports
from plone.app.layout.viewlets.interfaces import IAboveContent
from plone.app.layout.viewlets.interfaces import IBelowContent
from plone.app.layout.viewlets.interfaces import IPortalTop

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from zope.component.hooks import getSite


class contactViewlet(ViewletBase):
    """A viewlet showing some site_properties
    """
    
    index = ViewPageTemplateFile('templates/contactviewlet.pt')

    @property
    def available(self):
        return bool(self.getContact())
        
    def getContact(self):    
        registry = getUtility(IRegistry)
        if registry != 0:
            data = {
                'name' :  registry['judo.theme.interfaces.IJudoThemeSettings.contactName'],
                'address' : registry['judo.theme.interfaces.IJudoThemeSettings.contactAddress'],
                'phone' : registry['judo.theme.interfaces.IJudoThemeSettings.contactPhone'],
                'email' : registry['judo.theme.interfaces.IJudoThemeSettings.contactEmail'],
            }
                
            return data


class colorViewlet(ViewletBase):
    """ a dummy viewlet containing the theme color"""
    
    index = ViewPageTemplateFile('templates/colorviewlet.pt')
    
    def available(self):
        return bool(self.getColor())
    
    def getColor(self):
        registry = getUtility(IRegistry)
        if registry != 0:
            color = registry['judo.theme.interfaces.IJudoThemeSettings.themeColor']
        return color
    
  
class dropdownViewlet(ViewletBase):
    """A dropdown menu with only one level
    """
    index = ViewPageTemplateFile('templates/dropdownviewlet.pt')
    
    def available(self):
        if self.dropdownMenu():
            return True
        else:
            return False

    def dropdownMenu(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        result = []
        site = getSite()
        path = site.getPhysicalPath()
        for parent in catalog(
            {'portal_type': ('Folder', 'Document','Event','News Item'),
             'path': dict(query=path, depth=1),
             'sort_on': 'getObjPositionInParent',}
            ):
            if parent.exclude_from_nav:
                continue         
            parentpath = parent.getPath()
            children = catalog(path={'query': parentpath, 'depth': 1}, sort_on = 'getObjPositionInParent')
            result.append(
                dict(parent=parent,
                      children=children)
            )
        return result