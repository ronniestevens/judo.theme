from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting

from plone.testing import z2

from zope.configuration import xmlconfig


class JudothemeLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load ZCML
        import judo.theme
        xmlconfig.file(
            'configure.zcml',
            judo.theme,
            context=configurationContext
        )

        # Install products that use an old-style initialize() function
        #z2.installProduct(app, 'Products.PloneFormGen')

#    def tearDownZope(self, app):
#        # Uninstall products installed above
#        z2.uninstallProduct(app, 'Products.PloneFormGen')

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'judo.theme:default')

JUDO_THEME_FIXTURE = JudothemeLayer()
JUDO_THEME_INTEGRATION_TESTING = IntegrationTesting(
    bases=(JUDO_THEME_FIXTURE,),
    name="JudothemeLayer:Integration"
)
